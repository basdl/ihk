from subprocess import call
import os

# path is a list of tuples, whereas (lon, lat)
def route(path):
  params = []
  i = 1
  for p in path:
    params.append("--lon%d=%f" % (i, p[0]))
    params.append("--lat%d=%f" % (i, p[1]))
    i+=1

  def_params = ["routino-2.3.2/web/bin/router", "--prefix=routino-2.3.2/web/data/", "--transport=car", "--quickest", "--profiles=routino-2.3.2/web/data/profiles.xml", "--translations=routino-2.3.2/web/data/translations.xml", "--output-stdout", "--quiet"]
  def_params += params
  call(def_params)
  
  # Now parse the file
  track = []
  try:
    for line in open("quickest.txt","r"):
      if not line.startswith("#"):
        lat,lon = [float(f) for f in line.split("\t")[0:2]]
        track.append((lon,lat))
    os.remove("quickest.txt")
  except IOError as e:
    print "Failed to read quickest.txt"
  return track
