#!/usr/bin/env python

import sys
import datetime
import json
import time
import plotter
import projection

zoom = int(sys.argv[1])

class Infix:
    def __init__(self, function):
        self.function = function
    def __ror__(self, other):
        return Infix(lambda x, self=self, other=other: self.function(other, x))
    def __or__(self, other):
        return self.function(other)
    def __rlshift__(self, other):
        return Infix(lambda x, self=self, other=other: self.function(other, x))
    def __rshift__(self, other):
        return self.function(other)
    def __call__(self, value1, value2):
        return self.function(value1, value2)

minus=Infix(lambda x,y: (x[0] - y[0], x[1] - y[1]))

def draw(paths, tile_x, tile_y):
  plot = plotter.Plotter(projection.getTileSize(),projection.getTileSize())
  plot.set_rgba(0,0,0,0.1)
  offs_x = tile_x*projection.getTileSize()
  offs_y = tile_y*projection.getTileSize()
  for path in paths:
    projected_path = [projection.project(p[1],p[0],zoom) |minus| (offs_x,offs_y) for p in path]
    plot.polyline(projected_path, False)
  return plot

def main(separator='\t'):
  for line in sys.stdin:
    tile,paths = line.strip().split(separator)
    tile = tile.strip("()").split(",")
    tile[1] = tile[1].strip()
    paths = json.loads(paths)
    draw(paths,int(tile[0]), int(tile[1])).save("tiles/" + "_".join(tile) + ".png")
    
    
if __name__ == "__main__":
    main()
