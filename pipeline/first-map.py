#!/usr/bin/env python
"""A more advanced Mapper, using Python iterators and generators."""

import sys
import datetime
import time

def intelligent_date_parse(date):
  try:
    return datetime.datetime.strptime(date[:-6], "%a %b %d %H:%M:%S")
  except:
    pass
    
  try:
    return datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S")
  except:
    pass

def main(separator='\t'):
  for line in sys.stdin:
    dataid,userid,date,lon,lat = line.strip().split(separator)
    # parse the date
    try:
      date = "%i" % time.mktime(intelligent_date_parse(date).timetuple())
      print separator.join([userid,date,lon,lat])
    except:
      pass

if __name__ == "__main__":
    main()
