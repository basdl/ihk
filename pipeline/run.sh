processors=32
zoom=15

python first-map.py < ../data/13.44723_48.57977_13.48371_48.56555 > /tmp/map
sort /tmp/map -o /tmp/map
python first-reduce.py < /tmp/map > /tmp/red
python second-map-route.py < /tmp/red > /tmp/routes


rm -rf /tmp/tmp
mkdir -p /tmp/tmp

# Tiles Routes
count=`wc -l < /tmp/routes`
splitsize=$(($count / $processors))
csplit -k -f /tmp/tmp/routes. /tmp/routes $splitsize {$(($processors + 1))}
for part in /tmp/tmp/routes.*; do
  bname=`basename $part`
  python third-map.py $zoom < $part > "/tmp/tmp/tiled.$bname" &
done
wait
rm -r /tmp/tmp/routes.*


# Sort all routes
cat /tmp/tmp/tiled.* | sort --parallel=$processors --buffer-size=1600M --o /tmp/tiled.routes
rm -rf /tmp/tmp/tiled.*
python third-reduce.py < /tmp/tiled.routes > /tmp/tiles

# Paint tiles
count=`wc -l < /tmp/tiles`
splitsize=$(($count / $processors))
csplit -k -f /tmp/tmp/paths. /tmp/tiles $splitsize {$(($processors + 1))}
for part in /tmp/tmp/paths.*; do
  bname=`basename $part`
  python fourth-map.py $zoom < $part &
done
wait
rm -r /tmp/tmp/paths.*
