import cairo

class Plotter:
  def __init__(self, width=1680, height=1050):
    self.width, self.height = width,height
    self.surface = cairo.ImageSurface(cairo.FORMAT_RGB24,width,height)
    self.ctx = cairo.Context(self.surface)
    # fill everyting with white
    self.ctx.new_path()
    self.ctx.set_source_rgb(1,1,1)
    self.ctx.rectangle(0,0,width,height)
    self.ctx.fill()  # fill current path
    
  def clear(self):
    self.ctx.set_source_rgb(1,1,1)
    self.ctx.rectangle(0,0,self.width,self.height)
    self.ctx.fill()  # fill current path
    
  def set_rgba(self, r,g,b,a):
    self.ctx.set_source_rgba(r,g,b,a)

  def line(self, x1,y1,x2,y2):
    self.ctx.move_to(x1,y1)
    self.ctx.line_to(x2, y2)
    self.ctx.stroke()
    
  def point(self, x,y):
    self.line(x,y,x+1,y+1)
    
  def polyline(self, arr, closed=True):
    arrlen = len(arr)
    for i,p in enumerate(arr):
      if i < len(arr) - 1:
        self.line(p[0], p[1], arr[(i+1)%arrlen][0], arr[(i+1)%arrlen][1])
      else:
        if closed:
          self.line(p[0], p[1], arr[(i+1)%arrlen][0], arr[(i+1)%arrlen][1])
      
  def strstr(self, txt,x,y):
    self.ctx.set_source_rgba(0,0,0,1)  # black
    self.ctx.select_font_face("Sans", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
    self.ctx.set_font_size(16)
    self.ctx.set_antialias(True)
    x_off, y_off, tw, th = self.ctx.text_extents(txt)[:4]
    self.ctx.move_to(x,y-th)
    self.ctx.show_text(txt)
      
  def save(self, outp):
    self.surface.write_to_png (outp) # Output to PNG    
