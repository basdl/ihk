import math

def getTileSize():
  return 250

def getAmountTilesX(zoom):
  if zoom < 19:
      return 1 << zoom
  else:
    return -1

def project(latitude, longitude, zoom):
  x = int(math.floor((longitude + 180) / 360 * (getAmountTilesX(zoom) * getTileSize())))
  sinLat = math.sin(latitude * math.pi / 180.0)
  y = int((getAmountTilesX(zoom) * getTileSize() * (0.5 - math.log((1 + sinLat) / (1 - sinLat)) / (4 * math.pi))))
  return (x,y)
