#!/usr/bin/env python

import sys
import datetime
import json
import time
import plotter
import projection

zoom = int(sys.argv[1])

class Infix:
    def __init__(self, function):
        self.function = function
    def __ror__(self, other):
        return Infix(lambda x, self=self, other=other: self.function(other, x))
    def __or__(self, other):
        return self.function(other)
    def __rlshift__(self, other):
        return Infix(lambda x, self=self, other=other: self.function(other, x))
    def __rshift__(self, other):
        return self.function(other)
    def __call__(self, value1, value2):
        return self.function(value1, value2)

minus=Infix(lambda x,y: (x[0] - y[0], x[1] - y[1]))

def draw(path, tile_x, tile_y, plot):
  plot.set_rgba(0,0,0,0.1)
  offs_x = tile_x*projection.getTileSize()
  offs_y = tile_y*projection.getTileSize()
  projected_path = [projection.project(p[1],p[0],zoom) |minus| (offs_x,offs_y) for p in path]
  plot.polyline(projected_path, False)

def main(separator='\t'):
  # Find all used tiles
  tiles = {}
  for line in open(sys.argv[2], "r"):
    userid,path = line.strip().split(separator)
    path_parsed = json.loads(path)
    for p in path_parsed:
      coords = projection.project(p[1], p[0], zoom)
      tile_x = coords[0]/projection.getTileSize()
      tile_y = coords[1]/projection.getTileSize()
      tile = (tile_x,tile_y)
      if not tile in tiles:
        tiles[tile] = plotter.Plotter(projection.getTileSize(),projection.getTileSize())
      
  # Draw all the routes
  for line in open(sys.argv[2], "r"):
    userid,path = line.strip().split(separator)
    path_parsed = json.loads(path)
    
    occ_tiles = set()
    for p in path_parsed:
      coords = projection.project(p[1], p[0], zoom)
      tile_x = coords[0]/projection.getTileSize()
      tile_y = coords[1]/projection.getTileSize()
      tile = (tile_x,tile_y)
      occ_tiles.add(tile)
    
    for tile in occ_tiles:
      draw(path, tile[0], tile[1], tiles[tile])

  # Save the tiles
  for tile,plot in tiles.iteritems():
    plot.save("tiles/" + "_".join(tile) + ".png")
  
  
  
if __name__ == "__main__":
    main()
