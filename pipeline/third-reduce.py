#!/usr/bin/env python

from operator import itemgetter
import sys
import json
import haversine

current_tile = None
current_paths = []

# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    tile,path = line.strip().split("\t")
    path = json.loads(path)
    
    # this IF-switch only works because Hadoop sorts map output
    # by key (here: word) before it is passed to the reducer
    if current_tile == tile:
        current_paths.append(path)
    else:
        if current_tile:
          print "%s\t%s" % (current_tile, json.dumps(current_paths, separators=(',',':')))
          
        current_paths = [path]
          
    current_tile = tile

if len(current_tile) > 0:
  print "%s\t%s" % (current_tile, json.dumps(current_paths, separators=(',',':')))
