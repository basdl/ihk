#!/usr/bin/env python

import sys
import os
import time
import json
import subprocess
from multiprocessing import *
import multiprocessing
import ctypes

def worker_func(line):
  userid,transport,path = line.strip().split('\t')
  path = json.loads(path)
  
  # print out the single paths
  return userid + "\t" + json.dumps(route(path, transport), separators=(',',':'))

def route(path, transport):
  params = []
  i = 1
  for p in path:
    params.append("--lon%d=%f" % (i, p[0]))
    params.append("--lat%d=%f" % (i, p[1]))
    i+=1

  def_params = ["../routino-2.3.2/web/bin/router-slim", "--prefix=../routino-2.3.2/web/data/bayern-"+transport, "--transport="+transport, "--shortest", "--profiles=../routino-2.3.2/web/data/profiles.xml", "--translations=../routino-2.3.2/web/data/translations.xml", "--output-stdout", "--quiet"]
  def_params += params
  
  # Now parse the output
  proc = subprocess.Popen(def_params,stdout=subprocess.PIPE)
  routed_path = []
  for line in iter(proc.stdout.readline,''):
     if not line.startswith("#"):
      lat, lon = line.split("\t")[0:2]
      routed_path.append([float(lon),float(lat)])
  return routed_path

def main(separator='\t'):
  pool = multiprocessing.Pool(None)
  
  lines = []
  for line in sys.stdin:
    lines.append(line)
  
  it = pool.imap_unordered(worker_func, lines, 50)
  
  for r in it:
    print r
  
  pool.close()
  pool.join()

if __name__ == "__main__":
    main()
    
    
    
