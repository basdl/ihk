#!/usr/bin/env python

import sys
import datetime
import time
import math
import json
import projection

zoom = int(sys.argv[1])

def main(separator='\t'):
  for line in sys.stdin:
    userid,path = line.strip().split(separator)
    path_parsed = json.loads(path)
    
    # find the correct tile
    tiles = set([])
    for p in path_parsed:
      coords = projection.project(p[1], p[0], zoom)
      tile_x = coords[0]/projection.getTileSize()
      tile_y = coords[1]/projection.getTileSize()
      tiles.add((tile_x,tile_y))
    
    for tile in tiles:
      print  str(tile) + "\t" + path

if __name__ == "__main__":
    main()
