#!/usr/bin/env python

from operator import itemgetter
import sys
import json
import haversine

current_user = None
current_list = []

def transport(speed, way):
  if speed > 15 or way > 5:
    return "motorcar"
  else:
    return "foot"

# takes as input an array of triples (date, longitude, latitude) and converts
# it to a set of single travel paths of that user
def split_user_paths(path):
  old_transport = None
  current_path = []
  for ix, [t,a,b] in enumerate(path):
    # so we can always look ahead to the next element
    if ix < len(path) - 1:
      dist = haversine.distance((a,b), path[ix+1][-2:])
      timediff = path[ix+1][0] - t 
      
      if dist > 0.0001 and timediff > 100:
        speed = 3600*dist/timediff
        current_path.append([a,b])
      
        
        if old_transport != transport(speed,dist):
          if len(current_path) > 1:
            yield (transport(speed,dist), current_path)
          current_path = [[a,b]]
          
        old_speed = transport(speed,dist)
      
  if len(current_path) > 1:
    yield (transport(speed), current_path)

# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    userid,date,lon,lat = line.strip().split("\t")
    userid = long(userid)
    date = long(date)
    lon = float(lon)
    lat = float(lat)

    # this IF-switch only works because Hadoop sorts map output
    # by key (here: word) before it is passed to the reducer
    if current_user == userid:
      current_list.append([date,lon,lat])
    else:
      if current_user:
          # sort the list by the date
          current_list = sorted(current_list, key=itemgetter(0))
          
          # split the parts into single travels and put out the single travels
          for travel in split_user_paths(current_list):
            print "%s\t%s\t%s" % (current_user, travel[0], json.dumps(travel[1], separators=(',',':')))
      current_user = userid
      current_list = [[date,lon,lat]]
