import sys
from datetime import datetime

inp = sys.argv[1]
rect = tuple([float(i) for i in sys.argv[2:6]])

(min_lon, max_lat, max_lon, min_lat) = rect
extr = open("_".join([str(i) for i in rect]),"w")
for line in open(inp, "r"):
  parts = line.split("\t")
  if len(parts) > 5:
    pid = parts[0]
    lon = float(parts[5])
    lat = float(parts[4])
    
    if lon >= min_lon and lon <= max_lon and lat >= min_lat and lat <= max_lat:
      userid = long(parts[1].split("@")[0])
      date = parts[7]
      extr.write(pid + "\t" + str(userid) + "\t" + str(date) + "\t" + str(lon) + "\t" + str(lat) + "\n")
    
extr.close()

