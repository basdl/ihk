/***************************************
 An automatically generated skeleton XML parser.

 Automatically generated by xsd-to-xmlparser.
 ***************************************/


#include <stdio.h>

#include "xmlparse.h"


/* The XML tag processing function prototypes */

static int xmlDeclaration_function(const char *_tag_,int _type_,const char *version,const char *encoding);
static int schemaType_function(const char *_tag_,int _type_,const char *elementFormDefault,const char *xmlns_xsd);
static int complexType_function(const char *_tag_,int _type_,const char *name);
static int attributeType_function(const char *_tag_,int _type_,const char *name,const char *type);
static int sequenceType_function(const char *_tag_,int _type_);
static int elementType_function(const char *_tag_,int _type_,const char *name,const char *type,const char *minOccurs,const char *maxOccurs);


/* The XML tag definitions */

/*+ The elementType type tag. +*/
static xmltag elementType_tag=
              {"xsd:element",
               4, {"name","type","minOccurs","maxOccurs"},
               elementType_function,
               {NULL}};

/*+ The sequenceType type tag. +*/
static xmltag sequenceType_tag=
              {"xsd:sequence",
               0, {NULL},
               sequenceType_function,
               {&elementType_tag,NULL}};

/*+ The attributeType type tag. +*/
static xmltag attributeType_tag=
              {"xsd:attribute",
               2, {"name","type"},
               attributeType_function,
               {NULL}};

/*+ The complexType type tag. +*/
static xmltag complexType_tag=
              {"xsd:complexType",
               1, {"name"},
               complexType_function,
               {&sequenceType_tag,&attributeType_tag,NULL}};

/*+ The schemaType type tag. +*/
static xmltag schemaType_tag=
              {"xsd:schema",
               2, {"elementFormDefault","xmlns:xsd"},
               schemaType_function,
               {&elementType_tag,&complexType_tag,NULL}};

/*+ The xmlDeclaration type tag. +*/
static xmltag xmlDeclaration_tag=
              {"xml",
               2, {"version","encoding"},
               xmlDeclaration_function,
               {NULL}};


/*+ The complete set of tags at the top level. +*/
static xmltag *xml_toplevel_tags[]={&xmlDeclaration_tag,&schemaType_tag,NULL};


/* The XML tag processing functions */


/*++++++++++++++++++++++++++++++++++++++
  The function that is called when the elementType XSD type is seen

  int elementType_function Returns 0 if no error occured or something else otherwise.

  const char *_tag_ Set to the name of the element tag that triggered this function call.

  int _type_ Set to XMLPARSE_TAG_START at the start of a tag and/or XMLPARSE_TAG_END at the end of a tag.

  const char *name The contents of the 'name' attribute (or NULL if not defined).

  const char *type The contents of the 'type' attribute (or NULL if not defined).

  const char *minOccurs The contents of the 'minOccurs' attribute (or NULL if not defined).

  const char *maxOccurs The contents of the 'maxOccurs' attribute (or NULL if not defined).
  ++++++++++++++++++++++++++++++++++++++*/

static int elementType_function(const char *_tag_,int _type_,const char *name,const char *type,const char *minOccurs,const char *maxOccurs)
{
 printf("<%s%s",(_type_==XMLPARSE_TAG_END)?"/":"",_tag_);
 if(name) printf(" name=\"%s\"",ParseXML_Encode_Safe_XML(name));
 if(type) printf(" type=\"%s\"",ParseXML_Encode_Safe_XML(type));
 if(minOccurs) printf(" minOccurs=\"%s\"",ParseXML_Encode_Safe_XML(minOccurs));
 if(maxOccurs) printf(" maxOccurs=\"%s\"",ParseXML_Encode_Safe_XML(maxOccurs));
 printf("%s>\n",(_type_==(XMLPARSE_TAG_START|XMLPARSE_TAG_END))?" /":"");
 return(0);
}


/*++++++++++++++++++++++++++++++++++++++
  The function that is called when the sequenceType XSD type is seen

  int sequenceType_function Returns 0 if no error occured or something else otherwise.

  const char *_tag_ Set to the name of the element tag that triggered this function call.

  int _type_ Set to XMLPARSE_TAG_START at the start of a tag and/or XMLPARSE_TAG_END at the end of a tag.
  ++++++++++++++++++++++++++++++++++++++*/

static int sequenceType_function(const char *_tag_,int _type_)
{
 printf("<%s%s",(_type_==XMLPARSE_TAG_END)?"/":"",_tag_);
 printf("%s>\n",(_type_==(XMLPARSE_TAG_START|XMLPARSE_TAG_END))?" /":"");
 return(0);
}


/*++++++++++++++++++++++++++++++++++++++
  The function that is called when the attributeType XSD type is seen

  int attributeType_function Returns 0 if no error occured or something else otherwise.

  const char *_tag_ Set to the name of the element tag that triggered this function call.

  int _type_ Set to XMLPARSE_TAG_START at the start of a tag and/or XMLPARSE_TAG_END at the end of a tag.

  const char *name The contents of the 'name' attribute (or NULL if not defined).

  const char *type The contents of the 'type' attribute (or NULL if not defined).
  ++++++++++++++++++++++++++++++++++++++*/

static int attributeType_function(const char *_tag_,int _type_,const char *name,const char *type)
{
 printf("<%s%s",(_type_==XMLPARSE_TAG_END)?"/":"",_tag_);
 if(name) printf(" name=\"%s\"",ParseXML_Encode_Safe_XML(name));
 if(type) printf(" type=\"%s\"",ParseXML_Encode_Safe_XML(type));
 printf("%s>\n",(_type_==(XMLPARSE_TAG_START|XMLPARSE_TAG_END))?" /":"");
 return(0);
}


/*++++++++++++++++++++++++++++++++++++++
  The function that is called when the complexType XSD type is seen

  int complexType_function Returns 0 if no error occured or something else otherwise.

  const char *_tag_ Set to the name of the element tag that triggered this function call.

  int _type_ Set to XMLPARSE_TAG_START at the start of a tag and/or XMLPARSE_TAG_END at the end of a tag.

  const char *name The contents of the 'name' attribute (or NULL if not defined).
  ++++++++++++++++++++++++++++++++++++++*/

static int complexType_function(const char *_tag_,int _type_,const char *name)
{
 printf("<%s%s",(_type_==XMLPARSE_TAG_END)?"/":"",_tag_);
 if(name) printf(" name=\"%s\"",ParseXML_Encode_Safe_XML(name));
 printf("%s>\n",(_type_==(XMLPARSE_TAG_START|XMLPARSE_TAG_END))?" /":"");
 return(0);
}


/*++++++++++++++++++++++++++++++++++++++
  The function that is called when the schemaType XSD type is seen

  int schemaType_function Returns 0 if no error occured or something else otherwise.

  const char *_tag_ Set to the name of the element tag that triggered this function call.

  int _type_ Set to XMLPARSE_TAG_START at the start of a tag and/or XMLPARSE_TAG_END at the end of a tag.

  const char *elementFormDefault The contents of the 'elementFormDefault' attribute (or NULL if not defined).

  const char *xmlns_xsd The contents of the 'xmlns:xsd' attribute (or NULL if not defined).
  ++++++++++++++++++++++++++++++++++++++*/

static int schemaType_function(const char *_tag_,int _type_,const char *elementFormDefault,const char *xmlns_xsd)
{
 printf("<%s%s",(_type_==XMLPARSE_TAG_END)?"/":"",_tag_);
 if(elementFormDefault) printf(" elementFormDefault=\"%s\"",ParseXML_Encode_Safe_XML(elementFormDefault));
 if(xmlns_xsd) printf(" xmlns:xsd=\"%s\"",ParseXML_Encode_Safe_XML(xmlns_xsd));
 printf("%s>\n",(_type_==(XMLPARSE_TAG_START|XMLPARSE_TAG_END))?" /":"");
 return(0);
}


/*++++++++++++++++++++++++++++++++++++++
  The function that is called when the XML declaration is seen

  int xmlDeclaration_function Returns 0 if no error occured or something else otherwise.

  const char *_tag_ Set to the name of the element tag that triggered this function call.

  int _type_ Set to XMLPARSE_TAG_START at the start of a tag and/or XMLPARSE_TAG_END at the end of a tag.

  const char *version The contents of the 'version' attribute (or NULL if not defined).

  const char *encoding The contents of the 'encoding' attribute (or NULL if not defined).
  ++++++++++++++++++++++++++++++++++++++*/

static int xmlDeclaration_function(const char *_tag_,int _type_,const char *version,const char *encoding)
{
 printf("<?%s",_tag_);
 if(version) printf(" version=\"%s\"",ParseXML_Encode_Safe_XML(version));
 if(encoding) printf(" encoding=\"%s\"",ParseXML_Encode_Safe_XML(encoding));
 printf(" ?>\n");
 return(0);
}


/*++++++++++++++++++++++++++++++++++++++
  A skeleton XML parser.
  ++++++++++++++++++++++++++++++++++++++*/

int main(int argc,char **argv)
{
 if(ParseXML(stdin,xml_toplevel_tags,XMLPARSE_UNKNOWN_ATTR_WARN))
    return(1);
 else
    return(0);
}
