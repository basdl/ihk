import sys
import plotter
from datetime import datetime
import random
from os.path import basename
import haversine
import math
import route

inp = sys.argv[1]
width=int(sys.argv[2])
rect = [float(i) for i in basename(inp).split("_")]

height=int(abs(rect[1] - rect[3])*width/abs(rect[0] - rect[2]))
users = {}

image = plotter.Plotter(width,height)

def proj_scale(lon, lat):
  lon = lon - rect[0]
  lat = lat - rect[3]
  sx = abs(rect[0] - rect[2])
  sy = abs(rect[1] - rect[3])
  return [lon*(width/sx), height-lat*(width/sx)]

for line in open(inp,"r"):
  parts = line.split("\t")
  pid = long(parts[0])
  user = long(parts[1])
  date = datetime.strptime(parts[2], "%Y-%m-%d %H:%M:%S")
  lon = float(parts[3])
  lat = float(parts[4])
  if not user in users:
    users[user] = []
  users[user].append([date, pid, (lon,lat)])
  
  

def naive_following(percentage):
  for k,v in users.iteritems():
    if random.random() <= percentage:
      image.set_rgba(0,0,0,0.01)
      points = [x[2] for x in sorted(v, key=lambda point: point[0])]
      ppoints = [proj_scale(x[0], x[1]) for x in points]
      image.polyline(ppoints, False)
      
def tile_following(tiles_x, tiles_y,percentage=1.0):
  for k,v in users.iteritems():
    if random.random() <= percentage:
      image.set_rgba(0,0,0,0.1)
      points = [x[2] for x in sorted(v, key=lambda point: point[0])]
      ppoints = [proj_scale(x[0], x[1]) for x in points]
      ppoints = [(tiles_x*(int(x[0])/tiles_x), tiles_y*(int(x[1])/tiles_y)) for x in ppoints]
      
      image.polyline(ppoints, False)
      
def route_paths():
  for k,v in users.iteritems():
    image.set_rgba(0,0,0,0.05)
    points = [x[2] for x in sorted(v, key=lambda point: point[0])]
    for i in xrange(0, len(points), 90):
      track = route.route(points[i:i+90])
      ppoints = [proj_scale(x[0], x[1]) for x in track]
      image.polyline(ppoints, False)
      
  image.save("_".join([str(i) for i in rect]) + "route.png")
      
def tile_following_time(tiles_x, tiles_y,max_time_diff=60,maxtiles=7):
  for k,v in users.iteritems():
    image.set_rgba(0,0,0,0.2)
    points = sorted(v, key=lambda point: point[0])
    ppoints = [proj_scale(x[2][0], x[2][1]) for x in points]
    for p in ppoints:
      x,y = p
      image.point(x,y)
    
    ppoints = [(tiles_x*(int(x[0])/tiles_x), tiles_y*(int(x[1])/tiles_y)) for x in ppoints]
    
    for i,v in enumerate(points):
      if i < len(points) - 1:
        tdiff = points[i+1][0] - v[0]
        if tdiff.total_seconds() < max_time_diff and tdiff.total_seconds() > 0:
          if abs(ppoints[i][0] - ppoints[i+1][0]) < maxtiles*tiles_x and abs(ppoints[i][1] - ppoints[i+1][1]) < maxtiles*tiles_y:
            image.line(ppoints[i][0], ppoints[i][1], ppoints[i+1][0], ppoints[i+1][1])

def timeline(secondstart, secondsend, step):
  s = secondstart
  while s < secondsend:
    image.clear()
    tile_following_time(3,3,s,15)
    image.strstr(str(s) + " seconds", 100, 100)
    image.save("timeline/" + "_".join([str(i) for i in rect]) + ("_%03d" % s) + ".png")
    s += step
    
def pathlength(tilesstart, tilesend, step):
  s = tilesstart
  while s < tilesend:
    image.clear()
    tile_following_time(3,3,60*5,s)
    image.strstr("3x3 pixel tiles, maxlength " + str(s), 100, 100)
    image.save("pathlength/" + "_".join([str(i) for i in rect]) + ("_%03d" % s) + ".png")
    s += step
    
# The speed is defined in m/s
def speed(start, end, step):
  total_meters = haversine.distance(rect[0:2], rect[2:4])*1000
  tile_width,tile_height=3,3
  tiles_pixel = math.sqrt(tile_width**2 + tile_height**2)
  tiles_meter = tiles_pixel * total_meters/math.sqrt(width**2 + height**2)
  max_time = 60*5
  s = start
  while s < end:
    image.clear()
    tile_following_time(tile_width, tile_height, max_time, max_time*s/tiles_meter)
    image.save("speed/" + "_".join([str(i) for i in rect]) + ("_%03d" % s) + ".png")
    s+=step

#pathlength(1, 20, 1)
#timeline(0,60*15,15)
#speed(0.1,20,0.1)

route_paths()

